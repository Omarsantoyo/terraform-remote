terraform {
    required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "=2.46.0"
    }
  }
  backend "azurerm" {
    resource_group_name  = "resource-devops"
    storage_account_name = "omardoustorage"
    container_name       = "omardoucontainer"
    key                  = "terraform.tfstate"
  }
}
provider "azurerm" {
  features {}
}


 data "azurerm_resource_group" "my-rg" {
  name = "resource-devops"
}


 resource "azurerm_virtual_network" "network" {
  name                = "myVN"
  address_space       = ["10.0.0.0/16"]
  location            = "East US"
  resource_group_name = data.azurerm_resource_group.my-rg.name
}
